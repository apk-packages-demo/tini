alias Buildah.{Apk, Cmd, Print}

apk_tini_on_container = fn (container, options) ->
    Apk.packages_no_cache(container, [
        "tini"
    ], options)
    {tini_exec_, 0} = Cmd.run(container, ["sh", "-c", "command -v tini"])
    {_, 0} = Cmd.config(
        container,
        entrypoint: "[\"#{String.trim(tini_exec_)}\", \"--\"]"
    )
end

apk_tini_test = fn (container, image_ID, options) ->
    {_, 0} = Cmd.run(container, ["tini", "--version"], into: IO.stream(:stdio, :line))
    {_, 0} = Podman.Cmd.run(image_ID, ["tini", "--version"],
        tty: true, rm: true, into: IO.stream(:stdio, :line)
    )
    Apk.packages_no_cache(container, ["psmisc"], options)
    {_, 0} = Cmd.run(container, ["pstree"], into: IO.stream(:stdio, :line))
    {_, 0} = Podman.Cmd.run(image_ID, ["pstree"],
        tty: true, rm: true, into: IO.stream(:stdio, :line)
    )
end




{whoami_, 0} = System.cmd("whoami", [])
"root" = String.trim(whoami_)
{_, 0} = Cmd.version(into: IO.stream(:stdio, :line))
{_, 0} = Cmd.info(into: IO.stream(:stdio, :line))

# image_basename = "alpine:edge"
# _image = "localhost/" <> image_basename

# image_from = "docker.io/" <> image_basename
image_from = System.fetch_env!("IMAGE_FROM")
IO.puts(image_from)

# image = "localhost/" <> image_basename
# image = System.fetch_env!("IMAGE")
# IO.puts(image)

# image_to = "docker://#{System.fetch_env!("CI_REGISTRY_IMAGE")}/" <> image_basename
image_to = System.fetch_env!("IMAGE_TO")
IO.puts(image_to)

Buildah.from_push(
    image_from,
    apk_tini_on_container,
    apk_tini_test,
    image_to: image_to,
    quiet: System.get_env("QUIET")
)

Print.images()
